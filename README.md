# My Personal Website!

The codebase for my personal website, which you can [visit
here](https://noway.moe)!

Written using [Astro.js](https://astro.build/) as the main framework, with
[Tailwind CSS](https://tailwindcss.com/). MDX rendering and syntax highlighting
provided by [remark.js](https://github.com/remarkjs).

![Desktop view of home page of
noway.moe](https://cdn.discordapp.com/attachments/436012664003624970/1191572285971255387/nowaymoe_desktop_full.jpeg)

### Building

If you're interested in building this code, do the following:

```bash
git clone https://git.mami2.moe/akemi/noway.moe
cd noway.moe
npm install
npm run dev

# To view the compiled version
npm run preview

# Once you're ready to publish
npm run build
```

I use [Cloudflare Pages](https://pages.cloudflare.com) for hosting. It's free
for static sites, but there are many other options to consider including
[Vercel](https://vercel.com/), [Github](https://pages.github.com), and
[Netlify](https://www.netlify.com) all of which are also free and fast for
static webpages! Live on the edge, pick one you haven't tried!

### Licence

Feel free to fork this code and use it for your website! No need to credit me.

This code is licensed under AGPL, meaning that you must open source any forks of
this code. That way everyone can benefit from our collective efforts!

## Design

This site is designed to reuse pages as much as possible. There are only 4
particularly different page types:

 1. Home page
 2. Posts listing (blog listing, unix listing...)
 3. Markdown posts (blogs, contact page)
 4. Pinterest-style masonry (cat pics page)

In general, only 4 style sheets are necessary to cover all the pages, but each
individual page makes adjustments on top of the base.


This makes it easy to make the website very mobile-responsive. The home page has
the most complex responsive design. Images are attached below!

#### Desktop Windowed

<img
    height="800px"
    alt="Windowed desktop view of home page of noway.moe"
    src="https://cdn.discordapp.com/attachments/436012664003624970/1191572285648285767/nowaymoe_desktop_windowed.jpeg"
    />

#### iPad Vertical

<img
    height="800px"
    alt="Vertical iPad view of home page of noway.moe"
    src="https://cdn.discordapp.com/attachments/436012664003624970/1191572285329514576/nowaymoe_ipad_air.jpeg"
    />

#### iPhone 12

<img
    height="800px"
    alt="iPhone view of home page of noway.moe"
    src="https://cdn.discordapp.com/attachments/436012664003624970/1191572284977205378/nowaymoe_iphone_12_pro.jpeg"
    />

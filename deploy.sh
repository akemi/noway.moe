#!/usr/bin/env bash
# Uploads the website to Cloudflare Pages

if ! npm run build; then
  echo "BUILD FAILED: Not uploading website"
else
  export CLOUDFLARE_ACCOUNT_ID="$(pass show cloudflare/account_id)"
  npx wrangler pages deploy dist
fi

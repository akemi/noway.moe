---
title: 'Use a Ramdisk'
description: 'Why you should use ramdisks'
pubDate: 'Dec 29 2023'
heroText: 'RAM Disks'
---

# RAM Disks

Traditionally, storage is placed on an SSD or HDD, both of which are are types
of non-volatile storage. This means if your computer loses power or restarts,
the data will persist on the disk. In most cases, this is desired.

A RAM disk is a type of storage recorded in in RAM. The key difference is that
RAM is volatile storage.

Let's consider the pros and cons of using RAM for storage:

Pros:
 - Very fast, faster than the best NVMe drives
 - True uniform access

Cons:
 - Limited to the size of your RAM, typically smaller than an SSD or HDD
 - All data is lost on powerless and reboot

In short, if you have RAM to spare and aren't worried about losing power, a RAM
disk is far more performant. It's especially noticeable when you're doing a huge
amount of small reads/writes. In fact, the Chromium project [recommends using a
RAM disk](https://chromium.googlesource.com/chromium/src/+/main/docs/linux/build_instructions.md#using-tmpfs)
to build the browser.

## Downloads Folder in RAM

Aside from the speedup, I personally use the RAM disk for its psychological
benefits. Knowing the storage is volatile motivates me to clean it up.

One example is when making changes to a git repository, I'll often clone the
repository onto a RAM disk. This reminds me to make *very* frequent commits,
since I'm constantly a bit concerned my work will be wiped via power outage or
crash. But the outcome is good atomic-committing in git.

I also put my Downloads folder in the RAM. For one thing, partial downloads from
Chromium and Firefox don't matter, since neither can pick up where they left
off. This means you'll have to restart an interrupted download even if you are
using non-volatile storage. I've also found my Downloads folder gets really
messy if I don't do this. Wiping it on reboot reminds me to move files into
their proper folders.

The following command will symlink your Downloads folder to the RAM disk. Most
programs will never notice the difference:

```bash
rmdir ~/Downloads
ln -s /dev/shm/ ~/Downloads
```

## Setting Up a RAM Disk

### Shared Memory

If you're on Linux, you already have a RAM disk! The path `/dev/shm` is found on
all Linux systems. It stands for "shared memory" and is intended as a space for
users and programs to share data. Very few programs or users actually do this,
but it means that directory is accessible for writing by all users including
you.

To verify a directory is mounted on a RAM disk, use the following command:

```bash
df -h .
# OR if you know the path
df -h /dev/shm
```

We care about what the `Filesystem` columns reports. It should be `tmpfs`,
meaning "temporary file system". For example mine looks like:

```
Filesystem      Size  Used Avail Use% Mounted on
tmpfs            16G  4.0G   12G  26% /dev/shm
```

We can also see the size of the RAM disk. In this case it's 16GB. By default,
Linux mounts half the system RAM for `/dev/shm`. This memory is only used if the
system needs to use it, so initially it takes up 0GB. The "size" is the maximum
amount of memory the file system can take up.

### Mount a RAM Disk

The particularly adventurous can also mount their own RAM disk. The `mount`
command bundled with Linux can do this. You will need root privileges.

The following mounts a 1GB RAM disk onto the directory `/home/emiliko/mnt`:

```bash
sudo mount -t tmpfs -o uid=1000,size=1g tmpfs /home/emiliko/mnt
```

## Advanced Notes

While in this article I implied all temporary file systems are RAM disks and
vise versa, neither is technically true. It's possible to mount `tmpfs` on
non-volatile storage, and many servers choose to do this. Similarly, a RAM disk
can be used for a normal file system, like btrfs.

However, for personal use, you'll never want to disconnect the terms `tmpfs` and
RAM disk. Using one without the other doesn't make any sense on a personal
computer and is unusual on a shared computer too.

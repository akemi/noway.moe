---
title: 'DuckieTown - Pre-Lab 1'
description: 'Getting ready for CMPUT 412'
pubDate: 'Jan 11 2023'
heroImage: '/images/duckietown/dashboard_motors_spin.avif'
---


# Week 1 - Before the first lab

Mostly did setup stuff. No programming yet

## Connecting to the robots

As it turns out, Duckietown needs support for `.local` name resolution. This
doesn't happen by default if using systemd-resolved. This
[stackexchange](https://unix.stackexchange.com/questions/43762/how-do-i-get-to-use-local-hostnames-with-arch-linux#146025)
provides a good overview

Additionally, use firewalld to unblock UDP on port 5353

```bash
firewall-cmd --zone=public --permanent --add-port=5353/udp
```

It likely won't work with `mdns_minimal` so instead use `mdns` in
`/etc/nsswitch.conf` and create another file `/etc/mdns.allow` with the lines

```
.local.
.local
```

See section 4 of the
[ArchWiki](https://wiki.archlinux.org/title/avahi#Troubleshooting) for more info

## Webpage

I used the markdown notes setup I've been working on for this site the past week
and slightly reworked it to suit the DuckieTown blog you're reading right now!
This also finished up a lot of styling problems that remained with the notes

## GUI Tools

As it turns out, I didn't have xhost installed, so of course the camera didn't
work.

```bash
please pacman -S xorg-xhost
```

With that, the camera now works at least... Use the following to get a camera
window running over xwayland. Make sure `dockerd` is running in systemd

```bash
systemctl start docker
dts start_gui_tools <addr-no-.local>
rqt_image_view  # Run this in the docker container
```

## Keyboard control

Keyboard controls also work!... except only in the non-graphic mode

```bash
dts duckiebot keyboard_control --cli <add-no-.local>
```

When the shell loads, it'll ask for one of `wasde`. Sometimes it won't register
keys right away. I found that after stopping with `e`, immediately using one of
the movement commands fails, since the duckiebot doesn't seem to have understood
it stopped. Instead send yet another `e` and then the movements should work

Conversely, if it's already moving with `w`, immediately using `asd` tends to
work. Additionally, if it's refusing to start moving forward with `w` and the
extra `e`s aren't helping, try doing the opposite movement with `s` then
immediately switch into `w`

## When things stop working

Sometimes, mDNS just doesn't feel it today with systemd-networkd. In this case
just gives up

```bash
systemctl stop systemd-networkd iwd
systemctl start NetworkManger
nmtui
```

Once everything is running, including docker, **open a new terminal**. Otherwise
there'll be errors like "QT Plugin not installed" and "Failed to open DISPLAY".
This has to do with updated environment variables

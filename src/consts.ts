// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = 'noway.moe';
export const SITE_DESCRIPTION = "Akemi's personal website!";

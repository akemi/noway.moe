import { defineConfig } from 'astro/config';
import mdx from '@astrojs/mdx';
import sitemap from '@astrojs/sitemap';
//import cloudflare from "@astrojs/cloudflare";
import remarkGfm from "remark-gfm";
import remarkRehype from "remark-rehype";
import rehypeSlug from "rehype-slug";
import rehypePrettyCode from "rehype-pretty-code";
import rehypeAutolinkHeadings from "rehype-autolink-headings";
import { s } from "hastscript";
import json from "./public/themes/github_light.json" assert { type: "json" };
import tailwind from "@astrojs/tailwind";

const options = {
  // Specify the theme to use or a custom theme json, in our case
  // it will be a moonlight-II theme from
  // https://github.com/atomiks/moonlight-vscode-theme/blob/master/src/moonlight-ii.json
  theme: json,
    // Callbacks to customize the output of the nodes
  onVisitLine(node) {
    // Prevent lines from collapsing in `display: grid` mode, and
    // allow empty lines to be copy/pasted
    if (node.children.length === 0) {
      node.children = [{type: 'text', value: ' '}];
    }
  },
  onVisitHighlightedLine(node) {
    // Adding a class to the highlighted line
    node.properties.className.push('highlighted');
  },
};

// https://astro.build/config
export default defineConfig({
  site: 'https://noway.moe',
  output: "static",
  integrations: [mdx(), sitemap(), tailwind()],

  build: {
    format: "directory",
    //inlineStylesheets: "auto",  // Seems to break local build
  },
  markdown: {
    gfm: true,
    syntaxHighlight: false,
    remarkPlugins: [
      remarkGfm,
      /* Conerts remark to rehype (md -> html) */
      [remarkRehype, { clobberPrefix: "" }],
      /*
       * rehypeSlug adds IDs to headings... which rehypeAutolinkHeadings then
       * extends to <a> tags
       */
      [rehypeSlug, {}],
      [rehypeAutolinkHeadings, {
    content(node) {
      return [
        s('svg', {viewBox: "0 0 16 16", class: "icon-link", width: "16", height: "16"}, [
          s('path', {d: "m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"})
        ])
      ]
    }
  }],
      /* Does the syntax highlighting and parsing on code */
      [rehypePrettyCode, options],
    ],
    //shikiConfig: {
    //  // Choose from Shiki's built-in themes (or add your own)
    //  // https://github.com/shikijs/shiki/blob/main/docs/themes.md
    //  theme: 'dracula',
    //  // Add custom languages
    //  // Note: Shiki has countless langs built-in, including .astro!
    //  // https://github.com/shikijs/shiki/blob/main/docs/languages.md
    //  langs: ["c", "ssh-config"],
    //  // Enable word wrap to prevent horizontal scrolling
    //  wrap: true,
    //},
  }
});
